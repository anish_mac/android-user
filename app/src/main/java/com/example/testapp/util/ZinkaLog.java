package com.example.testapp.util;

import android.util.Log;

//import com.crashlytics.android.Crashlytics;

//import in.zinka.androidclient.BuildConfig;

/**
 * Created by Ajith R Kamath on 24-08-2015.
 */
public class ZinkaLog {
    public static void errorLog(String tag, String msg) {
        Log.e(tag,msg);
    }
    public static void warnLog(String tag, String msg) {
            Log.w(tag,msg);
    }
    public static void debugLog(String tag, String msg) {
                Log.d(tag,msg);
    }
    public static void infoLog(String tag, String msg) {
                    Log.i(tag,msg);
    }
    public static void errorLog(String tag, String msg, Exception exception){

                        Log.e(tag,msg);
    }
}

