package com.example.testapp.util;

public class LanguageCode {
    public static final int TAMIL = 1;
    public static final int TELUGU = 2;
    public static final int KANNADA = 3;
    public static final int HINDI = 4;
    public static final int PUNJABI = 5;
    public static final int BENGALI = 6;
    public static final int ENGLISH = 7;
}
