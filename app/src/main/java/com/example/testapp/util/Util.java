package com.example.testapp.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Util{
    public static boolean checkAndRequestPermissions(Activity context, List<String> additionalPermissions) {
        ZinkaLog.debugLog("test", "checkAndRequestPermissions");
        int myVersion = Build.VERSION.SDK_INT;
        if (myVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {

            List<String> permissions = new ArrayList<>();
            permissions.add(android.Manifest.permission.INTERNET);
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissions.add(Manifest.permission.WAKE_LOCK);
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.ACCESS_NETWORK_STATE);
            //C2DM
            permissions.add(Manifest.permission.READ_PHONE_STATE);
            //ReadGservices
            //   permissions.add(Manifest.permission.READ_LOGS);
            permissions.add(Manifest.permission.VIBRATE);
            permissions.add(Manifest.permission.CALL_PHONE);
            permissions.add(Manifest.permission.RECEIVE_SMS);
            permissions.add(Manifest.permission.SEND_SMS);
            permissions.add(Manifest.permission.READ_SMS);
            //  permissions.add(Manifest.permission.SYSTEM_ALERT_WINDOW);
            permissions.add(Manifest.permission.CAMERA);
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);


            if (additionalPermissions != null && !additionalPermissions.isEmpty()) {
                for (String s : additionalPermissions) {
                    permissions.add(s);
                }
            }
            List<String> requiredPermissions = new ArrayList<>();
            for (int i = 0; i < permissions.size(); i++) {
                int result = ContextCompat.checkSelfPermission(context, permissions.get(i));
                if (result != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(permissions.get(i));
                }
            }
            if (!requiredPermissions.isEmpty()) {
                String debugStr = "";
                for (String s : requiredPermissions) {
                    debugStr += s;
                }
                ZinkaLog.debugLog("DF", "requiredPermissions List is not empty--" + debugStr);
                ActivityCompat.requestPermissions(context, requiredPermissions.toArray(new String[requiredPermissions.size()]), 1);
                return false;
            }
        }
        return true;
    }
}